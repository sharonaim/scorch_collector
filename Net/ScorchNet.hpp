#include <torch/torch.h>

namespace nn = torch::nn;
namespace F = torch::nn::functional;

class PolicyHead;
class ValueHead;
class ConvLayer;
class ResLayer;

class ScorchNetImpl : public nn::Module {
public:
	ScorchNetImpl(int64_t yBoard, int64_t xBoard, int64_t dims, int64_t actionSize) {
		this->conv = ConvLayer(yBoard, xBoard, dims);
		for (int i = 0; i < 5; i++) { // ResLayers init.
			this->resLayers[i] = ResLayer();
		}

		this->policy = PolicyHead(yBoard, xBoard, actionSize);
		this->value = ValueHead(yBoard, xBoard);
	}
    
    torch::Tensor forward(torch::Tensor x) {
		x = this->conv(x);
		for (auto& res : this->resLayers) {
			x = res(x);
		}

		torch::Tensor p = this->policy(x);
		torch::Tensor v = this->value(x);

    }

private:
	ConvLayer conv{ nullptr };
	ResLayer resLayers[5]{ nullptr };
	PolicyHead policy{ nullptr };
	ValueHead value{ nullptr };
protected:
};
TORCH_MODULE(ScorchNet);

class PolicyHeadImpl : public nn::Module {
public:
	PolicyHeadImpl(int64_t yBoard, int64_t xBoard, int64_t actionSize, int64_t inPlanes = 128,
		int64_t filters = 256) : viewArr{-1, yBoard * xBoard * filters} {
		this->conv = register_module("Conv2d", nn::Conv2d(nn::Conv2dOptions(inPlanes, filters, 1)));
		this->bn = register_module("Conv2d", nn::BatchNorm2d(nn::BatchNorm2dOptions(filters)));
		this->refViewArr = torch::IntArrayRef(this->viewArr, 2);
		this->func = register_module("Policy_Function", nn::Linear(yBoard * xBoard * filters, actionSize));
	}

	torch::Tensor forward(torch::Tensor x) {
		torch::Tensor v = F::relu(this->bn(this->conv(x)));
		v = v.view(this->refViewArr);
		v = this->func(v);
		return v;
	}

private:
	nn::Conv2d conv{ nullptr };
	nn::BatchNorm2d bn{ nullptr };
	nn::Linear func{ nullptr };
	int64_t viewArr[2];
	torch::IntArrayRef refViewArr;
};
TORCH_MODULE(PolicyHead);

class ValueHeadImpl : public nn::Module {
public:
	ValueHeadImpl(int64_t yBoard, int64_t xBoard, int64_t inPlanes = 128,
		int64_t filters = 128) : viewArr{ -1, yBoard * xBoard * filters } {
		this->conv = register_module("Conv2d", nn::Conv2d(nn::Conv2dOptions(inPlanes, filters, 1)));
		this->bn = register_module("Bn2d", nn::BatchNorm2d(nn::BatchNorm2dOptions(filters)));
		this->refViewArr = torch::IntArrayRef(this->viewArr, 2);

		this->ft_vec = register_module("Feature_Vector", nn::Linear(yBoard * xBoard * filters, 128));
		this->func = register_module("Value_Function", nn::Linear(128, 1));
	}

	torch::Tensor forward(torch::Tensor x) {
		torch::Tensor v = F::relu(this->bn(this->conv(x)));
		v = v.view(this->refViewArr);

		v = F::relu(this->ft_vec(v));
		v = torch::tanh(this->func(v));
		return v;
	}

private:
	nn::Conv2d conv{ nullptr };
	nn::BatchNorm2d bn{ nullptr };
	nn::Linear ft_vec{ nullptr };
	nn::Linear func{ nullptr };
	int64_t viewArr[2];
	torch::IntArrayRef refViewArr;
};
TORCH_MODULE(ValueHead);


class ConvLayerImpl : public nn::Module {
public:
	ConvLayerImpl(int64_t yBoard, int64_t xBoard, int64_t inPlanes = 1,
		int64_t outPlanes = 128, int64_t kernelSize = 3, int64_t padding = 1) : viewArr{ -1, inPlanes, yBoard, xBoard } {
		this->conv = register_module("Conv2d", nn::Conv2d(nn::Conv2dOptions(inPlanes, outPlanes, kernelSize).padding(padding)));
		this->bn = register_module("Bn2d", nn::BatchNorm2d(nn::BatchNorm2dOptions(outPlanes)));
		this->refViewArr = torch::IntArrayRef(this->viewArr, 4);
	}

	torch::Tensor forward(torch::Tensor x) {
		x = x.view(this->refViewArr);
		return F::relu(this->bn(this->conv(x)));
	}

private:
	nn::Conv2d conv{ nullptr };
	nn::BatchNorm2d bn{ nullptr };
	int64_t viewArr[4];
	torch::IntArrayRef refViewArr;
};
TORCH_MODULE(ConvLayer);


class ResLayerImpl : public nn::Module {
public:
	ResLayerImpl(int64_t inPlanes = 128, int64_t outPlanes = 128,
		int64_t kernelSize = 3, int64_t padding = 1) {
		this->conv1 = register_module("Conv2d", nn::Conv2d(nn::Conv2dOptions(inPlanes, outPlanes, kernelSize).padding(padding)));
		this->bn1 = register_module("Bn2d", nn::BatchNorm2d(nn::BatchNorm2dOptions(outPlanes)));

		this->conv2 = register_module("Conv2d", nn::Conv2d(nn::Conv2dOptions(outPlanes, outPlanes, kernelSize).padding(padding)));
		this->bn2 = register_module("Bn2d", nn::BatchNorm2d(nn::BatchNorm2dOptions(outPlanes)));
	}

	torch::Tensor forward(torch::Tensor x) {
		torch::Tensor residual = x;

		torch::Tensor proc = F::relu(this->bn1(this->conv1(x)));
		proc = this->bn2(this->conv2(proc));
		proc += residual;
		proc = F::relu(proc);
		return proc;
	}

private:
	nn::Conv2d conv1{ nullptr };
	nn::BatchNorm2d bn1{ nullptr };
	nn::Conv2d conv2{ nullptr };
	nn::BatchNorm2d bn2{ nullptr };
};
TORCH_MODULE(ResLayer);
