// #include <torch/torch.h>
#include <iostream>

#include "Games/TicTacToe.hpp"
#include "Tree/SearchActor.hpp"

int main() {
    GameParent* game = new TicTacToe();

    std::cout << *game << std::endl;
    while (!game->winner() && game->sample() != GameParent::overFlag) {
        game = SearchActor::mctsSearch(game);
        std::cout << *game << std::endl;
    }

    std::cout << "Winner: " << game->step(0) << std::endl;
    system("pause");
    return 0;
}
