#pragma once
#include <iostream>
#include <algorithm>
#include <iterator>
#include <vector>
#include <cstdlib>
#include <ctime>

#include "GameParent.hpp"

class TicTacToe : public GameParent {
public:
    TicTacToe() {
        this->turn = 1;
        for (int i = 0; i < 9; i++) {
            this->board[i] = 0;
            this->valid[i] = true;
        }
    }

    TicTacToe(TicTacToe &copy) { // copying a game state of TicTacToe.
        for (int i = 0; i < 9; i++) {
            this->board[i] = copy.board[i];
            this->valid[i] = copy.valid[i];
        }
        this->turn = copy.turn;
    }

    ~TicTacToe() { }

    GameParent* copy() {
        GameParent* copyVar = new TicTacToe(*this);
        return copyVar;
    }

    int step(int action) {
        bool validExists = false;
        for(int i = 0; i < 9; i++)
            validExists = validExists || this->valid[i];

        if (this->winner()) { // already won game.
            return this->turn;
        }
        if (!validExists) { // draw.
            return drawFlag; // special number for draw.
        }

        this->board[action] = this->turn;
        this->valid[action] = false; // cannot do same action again.

        if (this->winner()) { // if player won game.
            return this->turn;
        }

        this->turn = -this->turn;
        return 0; // game ongoing.
    }

    int sample() {
        std::vector<int> validIndeces;
        std::srand(std::time(nullptr));
        
        for (int i = 0; i < 9; i++) {
            if (this->valid[i])
                validIndeces.push_back(i);
        }

        if (validIndeces.empty())
            return overFlag;
        int randomVariable = std::rand() % validIndeces.size();
        return validIndeces[randomVariable];
    }

    bool winner() {
        int* cls = this->board;
 
        for (int i = 0; i < 3; i++) {
            if (cls[i] != 0 && cls[i] == cls[i + 3] && cls[i] == cls[i + 6])
                return true;
        }

        for (int i = 0; i < 3; i++) {
            if (cls[i * 3] != 0 && cls[i * 3] == cls[(i * 3) + 1] && cls[i * 3] == cls[(i * 3) + 2])
                return true;
        }

        if ((cls[0] != 0 && cls[0] == cls[4] && cls[0] == cls[8]) ||
            (cls[2] != 0 && cls[2] == cls[4] && cls[2] == cls[6]))
            return true;

        return false; // passed all checks and game not over.
    }

    bool* validActions() {
        return this->valid;
    }

    int getTurn() const {
        return this->turn;
    }

    void printFunc(std::ostream &stream) const {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                stream << this->board[(i * 3) + j] << " ";
            }
            stream << std::endl;
        }
        stream << std::endl;
    }

private:
    int board[9];
    bool valid[9];
    int turn;
};