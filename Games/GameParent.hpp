#pragma once
#include <iostream>

class GameParent
{
public:
    virtual ~GameParent() {} ;

    virtual GameParent* copy() = 0 ;

    virtual int step(int action) = 0 ;

    virtual int sample() = 0;

    virtual bool winner() = 0 ;

    virtual bool* validActions() = 0 ;

    virtual int getTurn() const = 0 ;

    virtual void printFunc(std::ostream &stream) const = 0 ;

    friend std::ostream& operator<<(std::ostream& stream, const GameParent& base) {
        base.printFunc(stream);
        return stream;
    }

    static const int drawFlag = -999;
    static const int overFlag = -420;
};

