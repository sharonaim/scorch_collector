#pragma once
#include <iostream>
#include <cstdlib>
#include <ctime>
#include <math.h>
#include <time.h>

#include "../Games/GameParent.hpp"
#include "../Tree/Node.hpp"

class SearchActor {
public:
    static GameParent* mctsSearch(GameParent* state) {
        std::srand(std::time(nullptr));
        Node* root = new Node(state, nullptr);
        clock_t startClock = clock();
        int loopCount = 0;
        int maxLoopCount = 9999;

        //while (maxLoopCount > loopCount) {
        while ((clock() - startClock) / CLOCKS_PER_SEC <= runtime) {
            Node* node = SearchActor::findUCB(root);

            node->expand(); // expanding for children.
            if (node->isaParent()) {
                node = node->getChild(std::rand() % node->getChildrenSize());
            }

            int reward = SearchActor::simulate(node);
            node->backpropagate(reward);
            loopCount++;
        }

        std::cout << loopCount << std::endl;
        GameParent* retState = root->favoriteChild()->getStateCopy();
        delete root;
        return retState;
    }

    static int simulate(Node* node) {
        GameParent* state = node->getStateCopy();
        int reward = 0;

        while (!reward) {
            reward = state->step(state->sample());
        }
        if (reward == GameParent::drawFlag)
            reward = 0; // settting true draw number.

        delete state;
        return reward;
    }

    static Node* findUCB(Node* root) {
        Node* proc = root;
        while (proc->isaParent()) {
            std::vector<Node*> &refChildren = proc->getChildren();
            int totalVisit = proc->getVisit();
            float maxUcbValue = -1;
            int maxIndex = 0;

            for (std::size_t i = 0; i < refChildren.size(); i++) { // finding most promising UCB child.
                double currValue = SearchActor::ucbForluma(totalVisit, refChildren[i]->getVisit(), refChildren[i]->getScore());
                if (currValue > maxUcbValue) {
                    maxUcbValue = currValue;
                    maxIndex = i;
                }
            }
            proc = proc->getChild(maxIndex);
        }
        return proc;
    }

    static double ucbForluma (int totalVisit, int nodeVisit, int nodeScore) {
        return (double(nodeScore) / (nodeVisit + int(1))) + 3 * sqrt(log(totalVisit) / (nodeVisit + int(1))); // C = sqrt(2).
    }

private:
    static const int runtime = 1;
};
