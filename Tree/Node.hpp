#pragma once
#include <vector>

#include "../Games/GameParent.hpp"
#include "../Games/TicTacToe.hpp"

class Node {
public:
    Node() : state(nullptr), parent(nullptr), scoreCount(0), visitCount(0) {}

    Node(GameParent* state = nullptr, Node* parent = nullptr) {
        this->state = state;
        this->parent = parent;
        this->visitCount = 0;
        this->scoreCount = 0;
    }

    Node(const Node &copy) {
        this->state = copy.state->copy();
        this->parent = copy.parent;
        this->visitCount = 0;
        this->scoreCount = 0;
    }

    ~Node() {
        delete this->state; // freeing the game.
        for (auto node : this->children) {
            delete node;
        }
    }

    void expand() { // searching for new states.
        if (!this->children.empty()) {
            return;
        }

        bool* valid = this->state->validActions(); // getting valid actions.
        for (int i = 0; i < 9; i++) {
            if (valid[i]) { // valid action.
                Node* childNode = new Node(this->state->copy(), this);
                childNode->state->step(i); // doing action on copy.
                this->children.push_back(childNode); // pushing child node with computed next state.
            }
        }
    }

    void backpropagate(int reward) {
        if (reward != this->state->getTurn()) { // like that we add if the reward is for the opponent.
            this->scoreCount++; // as the action that lead to the current state is taken by the opponent.
        } // and he should want to take it.

        this->visitCount++;
        if (this->parent != nullptr)
            this->parent->backpropagate(reward);
    }

    Node* favoriteChild() {
        int index = 0;
        int maxVisit = 0;

        for (std::size_t i = 0; i < this->children.size(); i++) {
            if (maxVisit < this->children[i]->visitCount) {
                maxVisit = this->children[i]->visitCount;
                index = i;
            }
        }
        return this->children[index];
    }

    bool isaParent() const {
        return !this->children.empty();
    }

    std::vector<Node*>& getChildren() {
        return this->children;
    }

    Node* getChild(int index) {
        return this->children[index];
    }

    int getChildrenSize() {
        return this->children.size();
    }

    GameParent* getStateCopy() {
        GameParent* state = this->state->copy();
        return state;
    }

    int getVisit() {
        return this->visitCount;
    }

    int getScore() {
        return this->scoreCount;
    }

private:
    std::vector<Node*> children;
    GameParent* state;
    Node* parent;
    float scoreCount;
    int visitCount;
};
